import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-image-item',
  templateUrl: './image-item.component.html',
  styleUrls: ['./image-item.component.css']
})
export class ImageItemComponent  {
  @Input() title:string  = '';
  @Input() url:string  = ''
  constructor() { }



}
