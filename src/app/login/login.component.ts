import {Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  password: string = 'roman';
  userPassword: string = '';
  showForm = false;

  constructor() { }

  checkPassword(){
    return  this.userPassword !== this.password
  }

  toToggleForm() {
    return this.showForm = !this.showForm;
  }




}
