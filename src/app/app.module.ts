import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskNumbersComponent } from './task-numbers/task-numbers.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LoginComponent } from './login/login.component';
import { FormComponent } from './form/form.component';
import { ImageItemComponent } from './image-item/image-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskNumbersComponent,
    GalleryComponent,
    LoginComponent,
    FormComponent,
    ImageItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
