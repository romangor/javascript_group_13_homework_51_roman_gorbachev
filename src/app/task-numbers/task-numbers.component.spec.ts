import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskNumbersComponent } from './task-numbers.component';

describe('TaskNumbersComponent', () => {
  let component: TaskNumbersComponent;
  let fixture: ComponentFixture<TaskNumbersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskNumbersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskNumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
