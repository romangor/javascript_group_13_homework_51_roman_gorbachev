import { Component } from '@angular/core';

@Component({
  selector: 'app-task-numbers',
  templateUrl: './task-numbers.component.html',
  styleUrls: ['./task-numbers.component.css']
})
export class TaskNumbersComponent {

  numbers: number [] = [];

  constructor() {
    this.numbers = this.addNumbers();
  }

  generateNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  addNumbers() {
    this.numbers = [];
    for (let i = 0; i < 5; i++) {
      let number = this.generateNumber(5, 36)
      this.numbers.push(number);
    }
    return this.numbers.sort(function (a, b) {return a - b});
  }
}
